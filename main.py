#!/usr/bin/python/env 
import os.path
import os
import argparse

def get_arguments():
    parser = argparse.ArgumentParser(description='usermake help.')
    parser.add_argument('-u', '--user', dest='is_user',
                        default=False,
                        action='store_true',
                        help='Create usernames from file.')
    parser.add_argument('-p', '--password', dest='is_password',
                        default=False,
                        action='store_true',
                        help='Create passwords from file.')
    parser.add_argument('-f', '--file', dest='names_file',
                        help='File containing the names to use.')
    parser.add_argument("-o", "--output", dest="out_file",
                        action='store_true',
                        default='out.txt',
                        help="Output file to store the results. Default = out.txt ")

    args = parser.parse_args()

    return args

def get_name_list(name_file):
    '''function to return a list of name
        For each user a list containing the first name and last name 
        is added to the returned list'''
    #opening file to read the names
    with open(name_file,'r') as file_name:
        list = file_name.readlines()
    
        #Here we remove the \n at the end of the lastname
        #and we create a list of list to store the name+lastname
        #This is maybe not very PEP compliant.
        name_list = [i[:-1].split(',') for i in list]
        return name_list

def write_names_to_file(file_to_write, name_list):
    '''method to write the names to the output file.
    By default the name is appended to the file'''
    with open(file_to_write, 'a') as file_to_write:
        for username in name_list:
            file_to_write.write(username+'\n')

def common_windows_active_directory_usernames(name_set):
    '''method that make username as usually found in active windows
        directory'''

    #define first name
    fname = name_set[0]
    #define last name
    lname = name_set[1]

    #create a list to store all the made up names
    #using several rules
    names = []
    names.append(fname + lname)
    names.append(fname + '.' + lname)
    names.append(fname[0] + '.' + lname)
    names.append(fname[0] + '-' + lname)
    names.append(fname[:3] + '.' + lname[:3])
    return names

def numerical_rules_usernames(name_set):
    #define first name
    fname = name_set[0]
    #define last name
    lname = name_set[1]

    #create a list to store all the made up names
    #using several rules
    names = []
    for i in range(0,10):
        names.append(fname + str(i))
        names.append(lname + str(i))
        names.append(fname[0] + '.' + lname + str(i))
        names.append(fname[0] + '-' + lname + str(i))
        names.append(fname[:3] + '.' + lname[:3] + str(i))
    return names


def delete_file(file_to_del):
    os.remove(file_to_del)

if __name__ == '__main__':

    # retrieving arguments
    arguments = get_arguments()

    names_file = arguments.names_file 
    out_file = arguments.out_file 
    
    is_user = arguments.is_user
    is_password = arguments.is_password

    print(is_user, is_password)
    
    #make a list containing the names
    name_list = get_name_list(names_file)
    #print(name_list)

    #if the file already exists, ask the user to overwrite it.
    if os.path.exists(out_file):
        overwrite_file = input('The file %s already exists, do you want to overwrite it? [y/n]:' %out_file)

        #Variable to check if input is valid
        can_continue = False
        while can_continue == False:
            if overwrite_file == 'y':
                delete_file(out_file)
                can_continue = True
            elif overwrite_file == 'n':
               can_continue = True 
            else:
                overwrite_file = input('Pleasen type y or n! ')

    #looping over all the names and start making usernames
    #a name_set is a list containing first name and last name
    for name_set in name_list:
        usernames = []
        usernames += common_windows_active_directory_usernames(name_set)
        usernames += numerical_rules_usernames(name_set)
        
        write_names_to_file(out_file,usernames)

    for username in usernames:
        print(username)
