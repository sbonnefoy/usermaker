# usermaker

This is a tool used to create tailored usernames in order to fuzz servers allowing user enumeration (SSH, AD, etc.)
The goal is to give all the usernames you find, and the tool will produce a list username combination, example:
John Doe will give you: johndoe, jdoe, john.doe, john-doe, etc.